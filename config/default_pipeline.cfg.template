# Configure available memory managers.
# =============================================================================

[MemoryManager]
size=3

# Numerical weather prediction memory manager gets 10 GB system memory.
1\name=NWP
1\size_MB=10240

# Memory manager that caches analysis results (10 MB are sufficient).
2\name=Analysis
2\size_MB=10

# Memory manager for trajectory data.
#3\name=Trajectories DF-T psfc_1000hPa_L62
#3\size_MB=10240


# Configure data processing pipelines.
# =============================================================================

[NWPPipeline]
size=2

# Example configuration for a deterministic ECMWF CF-NetCDF dataset.
# Specifiy a name for the dataset.
1\name=ECMWF DET EUR_LL015
# Sepcify the directory in which the data files are stored. If you specify
# a file filter (wildcards allowed), only those files that correspond to the
# filter will be used.
1\path=/your/path/data/mss/grid/ecmwf/netcdf
1\fileFilter=*ecmwf_forecast*EUR_LL015*.nc
# schedulerID can be "MultiThread" or "SingleThread".
1\schedulerID=MultiThread
# Specify the ID of a memory manager that is defined above.
1\memoryManagerID=NWP
# fileFormat can be "CF_NETCDF" or "ECMWF_GRIB".
1\fileFormat=CF_NETCDF
# Set enableRegridding to "true" if the regridding module should be activated.
1\enableRegridding=false
# Set "enableProbabilityRegionFilter" to "true" if you want to use the
# probability region detection module.
1\enableProbabilityRegionFilter=false

# Example configuration for an ensemble ECMWF CF-NetCDF dataset.
2\name=ECMWF ENS EUR_LL10
2\path=/your/path/data/mss/grid/ecmwf/netcdf
2\fileFilter=*ecmwf_ensemble_forecast*EUR_LL10*.nc
2\schedulerID=MultiThread
2\memoryManagerID=NWP
2\fileFormat=CF_NETCDF
2\enableRegridding=false
2\enableProbabilityRegionFilter=false

3\name=YOUR CF DATASET
3\path=/your/path/data/mss/grid/generic/netcdf
3\fileFilter=*
3\schedulerID=MultiThread
3\memoryManagerID=NWP
3\fileFormat=CF_NETCDF
3\enableRegridding=false
3\enableProbabilityRegionFilter=false


[LagrantoPipeline]
size=0

1\name=Lagranto ENS EUR_LL10 DF-T psfc_1000hPa_L62
1\ensemble=true
1\path=/your/path/data/trajectories/EUR_LL10/psfc_1000hPa_L62
1\ABLTrajectories=false
#1\schedulerID=SingleThread
1\schedulerID=MultiThread
1\memoryManagerID=Trajectories DF-T psfc_1000hPa_L62
